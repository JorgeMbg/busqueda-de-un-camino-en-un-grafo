/*
Copyright 2021 Jorge Martin-Benito Garcia
        Licensed under the Apache License, Version 2.0 (the "License");
        you may not use this file except in compliance with the License.
        You may obtain a copy of the License at
        http://www.apache.org/licenses/LICENSE-2.0
        Unless required by applicable law or agreed to in writing, software
        distributed under the License is distributed on an "AS IS" BASIS,
        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
        See the License for the specific language governing permissions and
        limitations under the License.
 */

package pr2.org;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.TreeSet;

public class Graph<V>{
    
    //Lista de adyacencia.
    private Map<V, Set<V>> adjacencyList = new HashMap<>();
    
    /******************************************************************
    * Añade el vértice ‘v‘ al grafo.
    *
    * @param v vértice a añadir.
    * @return ‘true‘ si no estaba anteriormente y ‘false‘ en caso
    * contrario.
    ******************************************************************/
    
    public boolean addVertex(V v){
        if (!this.adjacencyList.containsKey(v)) {
            this.adjacencyList.put(v, new TreeSet<V>());
            return true;
        }
        else return false;
    }

    /******************************************************************
    * Añade un arco entre los vértices ‘v1‘ y ‘v2‘ al grafo. En
    * caso de que no exista alguno de los vértices, lo añade
    * también.
    *
    * @param v1 el origen del arco.
    * @param v2 el destino del arco.
    * @return ‘true‘ si no existía el arco y ‘false‘ en caso contrario.
    ******************************************************************/

    public boolean addEdge(V v1, V v2){
        // ¿Existen los vértices?
        addVertex(v1);
        addVertex(v2);
        // Saco el set de arcos de v1.
        Set<V> v1list = this.adjacencyList.get(v1);
        // Añado v2 si no existe.
        boolean v1edge = v1list.add(v2);
        Set<V> v2list = this.adjacencyList.get(v2);
        v2list.add(v1);
        // Devuelvo true si el arco no existia
        if(v1edge == true)
            return true;
        else
            return false;
    }

    /******************************************************************
    * Obtiene el conjunto de vértices adyacentes a ‘v‘.
    *
    * @param v vértice del que se obtienen los adyacentes.
    * @return conjunto de vértices adyacentes.
    ******************************************************************/

    public Set<V> obtainAdjacents(V v) throws Exception{  
        Set<V> vlist;
            try{
                vlist = this.adjacencyList.get(v);
            }
            catch (Exception e) {
                throw e;
            }
        return vlist;
    }

    /******************************************************************
    * Comprueba si el grafo contiene el vértice dado.
    *
    * @param v vértice para el que se realiza la comprobación.
    * @return ‘true‘ si ‘v‘ es un vértice del grafo.
    ******************************************************************/
    
    public boolean containsVertex(V v){
            return (this.adjacencyList.containsKey(v));
    }

    /******************************************************************
    * Método ‘toString()‘ reescrito para la clase ‘Grafo.java‘.
    * @return una cadena de caracteres con la lista de adyacencia.
    ******************************************************************/

    @Override
    public String toString(){
        String graphString = "";
        // Itero sobre el HashMap y por cada vertice imprimo su lista de adyacencia.
        for ( Map.Entry<V,Set<V>> entry : adjacencyList.entrySet()) {
            //Iterar en un Map
            graphString += entry.getKey().toString() + ":";
            Iterator<V> it = entry.getValue().iterator();
            //Iterar en un Set
            while(it.hasNext()) {
                graphString+=" "+it.next().toString();
            }
            graphString += "\n";
            
        }
        return graphString;
    }

    /******************************************************************
    * Obtiene, en caso de que exista, un camino entre ‘v1‘ y ‘v2‘. En
    * caso contrario, devuelve ‘null‘.
    *
    * @param v1 el vértice origen.
    * @param v2 el vértice destino.
    * @return lista con la secuencia de vértices desde ‘v1‘ hasta ‘v2‘
    * pasando por arcos del grafo.
    ******************************************************************/

    public List<V> onePath(V v1, V v2){
        Map<V, V> traza = new HashMap<>();
        Map<V, Boolean> visitado = new HashMap<>();
        Stack<V> abierta = new Stack<>();
        abierta.push(v1);
        traza.put(v1, null); 
        boolean encontrado = false;
        while(!abierta.empty() && !encontrado) {
            V v = abierta.pop();
            visitado.put(v, true);
            if (v == v2) {
                encontrado = true;
            } else {
                Set<V> listaArcos = new TreeSet<>();
                try {
                    listaArcos = obtainAdjacents(v);
                } catch (Exception e) {
                    // Estoy seguro que v existe en el grafo, por lo que el error no deberia saltar.
                    System.out.println("ERROR");
                    return null;
                }
                Iterator<V> it = listaArcos.iterator();
                V vArco;
                while(it.hasNext()) {
                    vArco = it.next();
                    if (!visitado.containsKey(vArco)) {
                        abierta.push(vArco);
                        traza.put(vArco , v);
                    }   
                }
            }
        }
        List<V> miLista = new ArrayList<>();
        if (encontrado) {
            V v = v2; // Inicializo a D
            while(v != v1) // Hasta que el padre no sea v1
            {
                miLista.add(v);
                v = traza.get(v);
            }
            miLista.add(v); //Añado el primer vertice.
            Collections.reverse(miLista);
            return miLista;
        } else {
            return null;
        }
    }
}
