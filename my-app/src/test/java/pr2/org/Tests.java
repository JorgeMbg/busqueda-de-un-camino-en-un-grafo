/*
Copyright 2021 Jorge Martin-Benito Garcia
        Licensed under the Apache License, Version 2.0 (the "License");
        you may not use this file except in compliance with the License.
        You may obtain a copy of the License at
        http://www.apache.org/licenses/LICENSE-2.0
        Unless required by applicable law or agreed to in writing, software
        distributed under the License is distributed on an "AS IS" BASIS,
        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
        See the License for the specific language governing permissions and
        limitations under the License.
 */

package pr2.org;

import org.junit.Before;
import static org.junit.Assert.*;
import java.util.*;
import org.junit.Test;

/**
* Este test comprueba que el método ‘onePath(V v1, V v2)‘
* encuentra un camino entre ‘v1‘ y ‘v2‘ cuando existe.
*/

public class Tests{
    
    private Graph<Integer> miGrafo;

    @Before
    public void setup() {
        this.miGrafo = new Graph<>();
    }

    @Test
    public void exsisteElGrafo() {
        assertNotNull(this.miGrafo);
    }

    @Test
    public void existeVertice(){
        assertTrue(this.miGrafo.addVertex(1));
    }

    @Test
    public void compruebaSiHayVertice(){
        this.miGrafo.addVertex(1);
        assertTrue(this.miGrafo.containsVertex(1));
    }

    @Test
    public void compruebaSiNoHayVertice(){
        assertFalse(this.miGrafo.containsVertex(1));
    }

    @Test
    public void imprimeElGrafo(){
        Graph<Integer> g = new Graph<>();
        g.addEdge(1, 2);
        g.addEdge(3, 4);
        g.addEdge(1, 5);
        g.addEdge(5, 6);
        g.addEdge(6, 4);
        System.out.println(g.toString());
    }

    
    @Test
    public void onePathFindsAPath(){
        System.out.println("\nTest onePathFindsAPath");
        System.out.println("----------------------");

        // Se construye el grafo.
        Graph<Integer> g = new Graph<>();
        g.addEdge(1, 2);
        g.addEdge(3, 4);
        g.addEdge(1, 5);
        g.addEdge(5, 6);
        g.addEdge(6, 4);

        // Se construye el camino esperado.
        List<Integer> expectedPath = new ArrayList<>();
        expectedPath.add(1);
        expectedPath.add(5);
        expectedPath.add(6);
        expectedPath.add(4);
        //Se comprueba si el camino devuelto es igual al esperado.

        assertEquals(expectedPath, g.onePath(1, 4));
    }
    
}
